<?php

/**
 * @file
 * Rules integration for DPD.
 */

/**
 * Implements hook_rules_action_info().
 */
function commerce_dpd_rules_action_info() {
  return array(
    'commerce_dpd_create_shipment' => array(
      'label' => t('Create shipment'),
      'named parameter' => TRUE,
      'parameter' => array(
        'commerce_order' => array(
          'type' => 'commerce_order',
          'label' => t('Order'),
        ),
        'contact_name' => array(
          'type' => 'text',
          'label' => t('Contact name'),
        ),
        'contact_telephone' => array(
          'type' => 'text',
          'label' => t('Contact telephone'),
        ),
        'collection_date' => array(
          'type' => 'text',
          'label' => t('Collection date'),
        ),
        'collection_organisation' => array(
          'type' => 'text',
          'label' => t('Collection organisation'),
        ),
        'collection_country_code' => array(
          'type' => 'text',
          'label' => t('Collection country code'),
        ),
        'collection_postcode' => array(
          'type' => 'text',
          'label' => t('Collection postcode'),
        ),
        'collection_street' => array(
          'type' => 'text',
          'label' => t('Collection street'),
        ),
        'collection_locality' => array(
          'type' => 'text',
          'label' => t('Collection locality'),
        ),
        'collection_town' => array(
          'type' => 'text',
          'label' => t('Collectio town'),
        ),
        'collection_county' => array(
          'type' => 'text',
          'label' => t('Collection county'),
        ),
        'delivery_contact_name' => array(
          'type' => 'text',
          'label' => t('Delivery contact name'),
        ),
        'delivery_contact_telephone' => array(
          'type' => 'text',
          'label' => t('Delivery contanct telephone'),
        ),
        'delivery_organisation' => array(
          'type' => 'text',
          'label' => t('Delivery organisation'),
        ),
        'delivery_country_code' => array(
          'type' => 'text',
          'label' => t('Delivery country code'),
        ),
        'delivery_postcode' => array(
          'type' => 'text',
          'label' => t('Delivery postcode'),
        ),
        'delivery_street' => array(
          'type' => 'text',
          'label' => t('Delivery street'),
        ),
        'delivery_locality' => array(
          'type' => 'text',
          'label' => t('Delivery locality'),
        ),
        'delivery_town' => array(
          'type' => 'text',
          'label' => t('Delivery town'),
        ),
        'delivery_county' => array(
          'type' => 'text',
          'label' => t('Delivery county'),
        ),
        'notification_email' => array(
          'type' => 'text',
          'label' => t('Notification email'),
        ),
        'notification_mobile' => array(
          'type' => 'text',
          'label' => t('Notification mobile'),
        ),
        'shipping_ref_one' => array(
          'type' => 'text',
          'label' => t('Shipping reference one'),
        ),
        'shipping_ref_two' => array(
          'type' => 'text',
          'label' => t('Shipping reference two'),
        ),
        'shipping_ref_three' => array(
          'type' => 'text',
          'label' => t('Shipping reference three'),
        ),
      ),
      'provides' => array(
        'commerce_dpd_shipment_id' => array(
          'type' => 'integer',
          'label' => t('DPD Shipment Id'),
        ),
        'commerce_dpd_consolidated' => array(
          'type' => 'boolean',
          'label' => t('DPD Consolidated'),
        ),
        'commerce_dpd_consignment_number' => array(
          'type' => 'integer',
          'label' => t('DPD Consignment Number'),
        ),
        'commerce_dpd_parcel_number' => array(
          'type' => 'integer',
          'label' => t('DPD Parcel Number'),
        ),
        'commerce_dpd_collection_date' => array(
          'type' => 'text',
          'label' => t('DPD Collection Date'),
        ),
      ),
      'group' => t('Commerce DPD'),
    ),
  );
}
