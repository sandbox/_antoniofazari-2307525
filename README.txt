Commerce DPD
===============

Description
-----------

Commerce DPD module provides functionality for integrating Drupal Commerce with
the DPD shipping system.
(http://drupal.org/project/commerce_dpd).

Commerce DPD provides the following integration:
- Create and insert shipments into DPD.
- Retrieve labels for a shipment.
- Retrieve parcel number for tracking a shipment.
- Rules integration to turn commerce orders into DPD shipments.

Dependencies
------------

Commerce
Commerce Order
Commerce Customer
Rules


Installation
------------

Commerce DPD contains one module:

- Commerce DPD

Enable it along with its dependencies.


Configuration
-------------

- The main Commerce DPD admin page

  Home > Administration > Store > Shipping > DPD Settings
  (admin/commerce/config/shipping/dpd)


MAINTAINERS
-----------
Current maintainers:
 * Antonio Fazari (_antonioFazari) - https://www.drupal.org/u/_antoniofazari
 * Graham Bates (gigabates) - https://www.drupal.org/user/1257024

This project has been sponsored by:
 * Catch Digital
   Specialized in consulting and planning of Drupal powered sites, Catch Digital
   offers installation, development, theming, customization, and hosting
   to get you started. Visit https://www.catchdigital.com for more information.
