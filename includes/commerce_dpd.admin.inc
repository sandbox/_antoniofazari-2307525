<?php
/**
 * @file
 * DPD settings.
 */

/**
 * Settings form for DPD settings.
 */
function commerce_dpd_settings_form($form, &$form_state) {

  $form = array();

  $form['api_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('API Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['api_settings']['live_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Live settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['api_settings']['live_settings']['commerce_dpd_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#description' => t('Enter your DPD username.'),
    '#default_value' => variable_get('commerce_dpd_username', ''),
    '#required' => TRUE,
  );

  $form['api_settings']['live_settings']['commerce_dpd_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#description' => t('Enter your DPD password.'),
    '#default_value' => variable_get('commerce_dpd_password', ''),
    '#required' => TRUE,
  );

  $form['api_settings']['live_settings']['commerce_dpd_api_url'] = array(
    '#type' => 'textfield',
    '#title' => t('API Url'),
    '#description' => t('Enter the api url with the schema. Eg. https://api.dpd.co.uk.'),
    '#default_value' => variable_get('commerce_dpd_api_url', 'https://api.dpd.co.uk'),
    '#required' => TRUE,
  );

  $form['api_settings']['live_settings']['commerce_dpd_account_number'] = array(
    '#type' => 'textfield',
    '#title' => t('Account Number'),
    '#description' => t('Enter your DPD account number.'),
    '#default_value' => variable_get('commerce_dpd_account_number', ''),
    '#required' => TRUE,
  );

  $form['api_settings']['test_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Test settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['api_settings']['test_settings']['commerce_dpd_test_mode'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Test Mode'),
    '#default_value' => variable_get('commerce_dpd_test_mode', 0),
  );

  $form['api_settings']['test_settings']['commerce_dpd_test_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#description' => t('Enter the DPD test username.'),
    '#default_value' => variable_get('commerce_dpd_test_username', 'APITEST'),
    '#required' => TRUE,
  );

  $form['api_settings']['test_settings']['commerce_dpd_test_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#description' => t('Enter the DPD test password.'),
    '#default_value' => variable_get('commerce_dpd_test_password', 'TESTAPI'),
    '#required' => TRUE,
  );

  // Custom submit handler for checking values.
  $form['#submit'][] = 'commerce_dpd_settings_form_submit';

  return system_settings_form($form);
}

/**
 * Validate function for commerce_dpd_settings_form.
 */
function commerce_dpd_settings_form_submit($form, &$form_state) {

  // If the username or password has been updated, delete the session variable.
  if ($form_state['values']['commerce_dpd_username'] != variable_get('commerce_dpd_username', '') ||
    $form_state['values']['commerce_dpd_password'] != variable_get('commerce_dpd_password', '') ||
    $form_state['values']['commerce_dpd_test_mode'] != variable_get('commerce_dpd_test_mode', '')
  ) {
    // Set the variables with the latest form values to ensure the correct
    // session token is created.
    variable_set('commerce_dpd_username', $form_state['values']['commerce_dpd_username']);
    variable_set('commerce_dpd_password', $form_state['values']['commerce_dpd_password']);
    variable_set('commerce_dpd_test_mode', $form_state['values']['commerce_dpd_test_mode']);

    // Authenticate again to get a new geo session token.
    commerce_dpd_authenticate(TRUE);

    // Set a watchdog message in the logs.
    watchdog('commerce_dpd', 'Authentication: form details have changed get new geo session token.', array(), WATCHDOG_INFO);
  }
}
